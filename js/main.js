/**
 * AngularJS
 * @author Wahyu Sanjaya <wahyu.sanjaya@emeriocorp.com>
 */

var mainApp = angular.module('mainApp', [
  'ngRoute'
]);

mainApp.config(function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "templates/page1.html", controller: ""})
    // Pages
    .when("/page2", {templateUrl: "templates/page2.html", controller: ""})
    // else 404
    .otherwise("/404", {templateUrl: "templates/404.html", controller: ""});
});

mainApp.controller("globalController", function($scope, $http) {
  $scope.message = "In global controller";
  $scope.type = "Global";

  $scope.listData = {
    subjects: [{
        name: 'Physics',
        marks: 70
      },
      {
        name: 'Chemistry',
        marks: 80
      },
      {
        name: 'Math',
        marks: 65
      }
    ]
  }

  $http({
    method: 'GET',
    url: 'http://52.76.85.10/test/location.json'
  }).then(function successCallback(response) {
    console.log("successfully");
    $scope.listData = response;
  }, function errorCallback(error) {
    console.log(error);
  });

});

mainApp.controller("firstController", function($scope) {
  $scope.message = "In first controller";
});

mainApp.controller("secondController", function($scope) {
  $scope.message = "In second controller";
  $scope.type = "Second";
});

mainApp.factory('MathService', function() {
  var factory ={};
  factory.multiply = function (a, b) {
    return a*b
  }
  factory.bagi = function (a, b) {
    return a/b
  }
  factory.tambah = function (a, b) {
    return a+b
  }
  factory.kurang = function (a, b) {
    return a-b
  }
  return factory;
});

mainApp.service('CalcService', function(MathService) {
  this.square = function(a) {
      return MathService.multiply(a,a);
  }
  this.kali = function(a,b) {
      return MathService.multiply(a,b);
  }
  this.bagi = function(a,b) {
      return MathService.bagi(a,b);
  }
  this.tambah = function(a,b) {
      return MathService.tambah(a,b);
  }
  this.kurang = function(a,b) {
      return MathService.kurang(a,b);
  }
});

mainApp.controller('CalcController', function($scope, CalcService) {
  $scope.square = function() {
      $scope.result = CalcService.square($scope.number);
  }
  $scope.kali = function() {
      $scope.result = CalcService.kali($scope.number,$scope.number1);
  }
  $scope.bagi = function() {
      $scope.result = CalcService.bagi($scope.number,$scope.number1);
  }
  $scope.tambah = function() {
      $scope.result = CalcService.tambah($scope.number,$scope.number1);
  }
  $scope.kurang = function() {
      $scope.result = CalcService.kurang($scope.number,$scope.number1);
  }
});
